<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 *
 *
 *
 */

get_header();
get_sidebar();
?>
<?php if (is_home() && ! is_front_page() ) : ?>
	<h1><?php single_post_title(); ?></h1>
<?php else : ?>
	<h2><?php _e( 'Posts', 'dreambig' ); ?></h2>
<?php endif; ?>

<section class="col-sm-8">

<?php
	if ( have_posts() ) :


		while ( have_posts() ) : the_post();
			get_template_part( 'template-parts/content', get_post_format() );
		endwhile;

	the_posts_pagination(); //edit this with bootstrap...
	
	else :
		get_template_part( 'template-parts/content', 'none' );
	endif;
?>
</section>
<?php
get_footer();