<?php
/**
 * The template for displaying the header
 * 
 * Used for generating everything in the HTML head section and the navigation section
 * 
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>
<?php wp_head(); ?>
</head>
<body> <!--could the body be a bootstrap container? not best practice to do so...-->
    <div class="container-fluid">
        <header class="row header-row">
            <h1><a href="<?php echo esc_url( home_url( '/') ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
            <h2><?php bloginfo( 'description' ); ?></h2>
            <?php
                if ( function_exists( 'index_main_nav' ) ) {
                ?>
                    <div id="nav-container" class="container">
                    <?php index_main_nav(); ?>
                    </div>
                <?php
                } else {
                    echo '<p> menu\'s not here ¯\_(ツ)_/¯</p>';
                }
            ?>
        </header><!-- .header -->
        <div class="row row-eq-height">