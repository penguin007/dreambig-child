<?php
/**
 * The Template for displaying the footer
 * 
 * Contains the closing of the main div for content, footer, and other closing tags
 */

//not much going on here. I don't know what wp_footer(); adds in but It's needed I think
?>
    </div> <!-- .first row -->
    <div class="row">
        <footer class="footer">
            <p>&copy; Dream Development, LLC</p>
            <p>Proudly Powered by: <a href="https://www.wordpress.org" title="WordPress Website" target="_blank">WordPress</a></p>
        </footer>
    </div> <!-- .row second-->
    </div> <!-- .container -->
<?php wp_footer(); ?>
</body>
</html>