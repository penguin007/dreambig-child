<?php
/**
 * The template used for displaying page content
 */
?>
        <!-- the content is in an article tag because I felt it was appropriate, might need a bootstrap class -->
        <article id="post-<?php the_ID(); ?>">
            <?php the_content(); //the content just pulls the body of the blog post or page ?>
        </article> <!-- .article -->