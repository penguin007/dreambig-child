<?php
/**
 * The template for displaying 404 pages (not found)
 * 
 */

get_header(); 
get_sidebar();
?>

                <main>
                    <h3>
                        <?php _e( 'Page Not Found', 'dreambig' ); ?>
                    </h3>
                    <p>
                        <?php _e( 'This is probably not the page you are looking for...', 'dreambig' ); ?>
                    </p>
                </main>
<?php get_footer(); ?>