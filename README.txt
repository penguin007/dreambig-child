=== Dream Big Child ===
Contributers: Matthew Mills and Danielle Lough
Requires at least: WordPress 4.9.8
Version: 0.5
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
tags: traditional, ADA-compliant, mobile-ready

== Description ==

A simplistic theme more information can really go here but I'm not sure what to say

PLUGINS REQUIRED:
	-Enhanced media library: https://wordpress.org/plugins/enhanced-media-library/
	-Quotes Collection: https://wordpress.org/plugins/quotes-collection/
	-Simple-Document-Gallery: https://gitlab.com/penguin007/simple-document-gallery (zip files first)

== Copyright ==
Dream Big, Copyright 2018 Dream Development, LLC
Dream Big is distributed under the terms of the GNU GPLv2


This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

Dream Big uses the following open source projects:

HTML5 Shiv, Copyright 2014 Alexander Farkas
Licenses: MIT/GPLv2
Source: https://github.com/aFarkas/html5shiv

Bootstrap, Copyright 2011-2018 The Bootstrap Authors
License: MIT
Source: https://github.com/twbs/bootstrap

WP Bootstrap Navwalker, Copyright 2018, wp-bootstrap.org
License: GPLv3
Source: https://github.com/wp-bootstrap/wp-bootstrap-navwalker/

Bootstrap_wp_link_pages, Copyright 2016, Eric Binnion
License: none
Source: https://gist.github.com/ebinnion/7635465

== Changelog ==

= 0.1 =
* Released: October 10, 2018

= 0.2 =
* Released: October 11, 2018

= 0.5 =
* Released: November 1, 2018
